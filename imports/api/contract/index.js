
import {Mongo} from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const Contract = new Mongo.Collection('Contract');

// allow/deny rules
Contract.allow({
    insert(userId, doc) {
        return false;
    },
    update(userId, doc, fields, modifier) {
        return false;
    },
    remove(userId, doc) {
        return false;
    },
});

Contract.schema = new SimpleSchema({
    createdAt: {
        type: Date,
    },
    name: {
        type: String,
    },
});

Contract.attachSchema(Contract.schema);