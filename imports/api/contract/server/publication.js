// All links-related publications

import {Meteor} from 'meteor/meteor';
import {Contract} from "../index.js";

Meteor.publish('getAllContracts', function () {
    return Contract.find();
});
