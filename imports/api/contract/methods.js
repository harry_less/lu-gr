import {ValidatedMethod} from "meteor/mdg:validated-method";
import SimpleSchema from 'simpl-schema';
import {Contract} from "/imports/api/contract/index.js"

export const createContract = new ValidatedMethod({
    name: "createContract",
    validate: new SimpleSchema({}).validator(),
    run({id}) {
        Contract.insert({
            name: "New contract",
            createdAt: new Date()
        })
    }
});