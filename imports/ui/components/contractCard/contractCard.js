import "./contractCard.html"
import "./contractCard.css"
import moment from "moment";

Template.ContractCard.helpers({
    contractDate:function(date){
        return moment(date).format('DD/MM/YYYY HH:MM');
    },
    contractUrl:function(contractId){
        return FlowRouter.url("contractDetail", {contractId: contractId});
    },
});