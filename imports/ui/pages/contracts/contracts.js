import "./contracts.html";
import "./contracts.css";
import '/imports/ui/components/contractCard/contractCard.js'
import {createContract} from "/imports/api/contract/methods.js";
import {Meteor} from 'meteor/meteor';
import {Contract} from '/imports/api/contract/index.js';


Template.Contracts.events({
    'click .js-addContract': function (event, template) {
        let self = Template.instance();
        createContract.call({}, function (err, result) {
            console.log(err, result)
        });
        return true;
    },
});

Template.Contracts.helpers({
    contracts: function () {
        return Contract.find();
    },
})


Template.Contracts.onCreated(function () {
    Meteor.subscribe('getAllContracts');
});