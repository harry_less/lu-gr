import {FlowRouter} from 'meteor/kadira:flow-router';
import {BlazeLayout} from 'meteor/kadira:blaze-layout';

// Import needed templates
import '../../ui/layouts/mainLayout/mainLayout.js';

import "/imports/ui/pages/contractDetail/contractDetail.js"
import "/imports/ui/pages/contracts/contracts.js"
import "/imports/ui/pages/new_contract/new_contract.js"

// Set up all routes in the app
FlowRouter.route('/', {
    name: 'contracts',
    action() {
        BlazeLayout.render('MainLayout', {main: 'Contracts'});
    },
});

FlowRouter.route('/contract/:contractId', {
    name: 'contractDetail',
    action() {
        BlazeLayout.render('MainLayout', {main: 'ContractDetail'});
    },
});

FlowRouter.route('/new_contract', {
    name: 'new_contract',
    action() {
        BlazeLayout.render('MainLayout', {main: 'new_contract'});
    },
});


FlowRouter.notFound = {
    action() {
        BlazeLayout.render('App_body', {main: 'App_notFound'});
    },
};
