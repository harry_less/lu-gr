// Fill the DB with example data on startup

import {Meteor} from 'meteor/meteor';
import {Contract} from '/imports/api/contract/index.js';

Meteor.startup(() => {
    // if the Links collection is empty
    if (Contract.find().count() === 0) {
        const data = [
            {
                name: 'Fake Company',
                createdAt: new Date(),
            },
            {
                name: 'ALLO HOUSTON',
                createdAt: new Date(),
            },
            {
                name: 'Addineo',
                createdAt: new Date(),
            },
        ];
        data.forEach(function (contract) {
            Contract.insert(contract)
        });
    }
});
